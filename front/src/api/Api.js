import axios from 'axios';

const ip = '10.0.28.193';
const url = `http://${ip}:8080/api`;

export const getMemory = () => {
  return axios
    .get(`${url}/memories`, {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      }
    })
    .then(response => response.data);
};

export const postMemory = memory => {
  return axios.post(`${url}/memories`, memory).then(response => response.data);
};

export const deleteMemory = id => {
  return axios.delete(`${url}/memories/${id}`).then(response => response.data);
};

export const putMemory = (id, memory) => {
  return axios
    .put(`${url}/memories/${id}`, memory)
    .then(response => response.data);
};
