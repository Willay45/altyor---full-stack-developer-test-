import React, { useState } from 'react';
import { putMemory } from '../../api/Api';

const FormUpdateMemory = ({ memoryId }) => {
  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [capacity, setCapacity] = useState('');
  const [size, setSize] = useState('');
  const [description, setDescription] = useState('');
  const updateMemory = async () => {
    const memory = {
      name,
      price,
      capacity,
      size,
      description
    };
    try {
      await putMemory(memoryId, memory);
    } catch (error) {
      alert('Memory update error');
    }
  };

  return (
    <div className="components-form">
      <span className="title-container">
        <h1>Update a Memory</h1>
      </span>
      <p>Complete the entire form</p>
      <form className="form-container">
        <label htmlFor="name">Name</label>
        <input
          className="name"
          type="name"
          onChange={event => setName(event.target.value)}
        />

        <label htmlFor="number">Price (EUR)</label>
        <input
          className="price"
          type="number"
          onChange={event => setPrice(event.target.value)}
          min={0}
        />

        <label htmlFor="number">Storage capacity (Go)</label>
        <input
          className="capacity"
          type="number"
          onChange={event => setCapacity(event.target.value)}
          min={0}
        />

        <label htmlFor="number">Size (mm)</label>
        <input
          className="size"
          type="number"
          onChange={event => setSize(event.target.value)}
          min={0}
        />

        <label htmlFor="text">Description</label>
        <textarea
          className="description"
          name="text"
          id="text"
          cols="30"
          rows="10"
          onChange={event => setDescription(event.target.value)}
        />

        <div className="send-button">
          <input type="submit" value="Update" onClick={updateMemory} />
        </div>
      </form>
    </div>
  );
};

export default FormUpdateMemory;
