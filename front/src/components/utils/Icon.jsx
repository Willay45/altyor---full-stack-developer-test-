import React from 'react';

const Icon = ({ image, onClick, color }) => (
  <div onClick={onClick} style={{ color: `${color}` }}>
    <i className={`${image}`} />
  </div>
);

export default Icon;
