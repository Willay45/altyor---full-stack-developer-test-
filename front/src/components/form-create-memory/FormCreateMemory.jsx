import React, { useState } from 'react';
import './FormCreateMemory.css';
import { postMemory } from '../../api/Api';

const FormCreateMemory = () => {
  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [capacity, setCapacity] = useState('');
  const [size, setSize] = useState('');
  const [description, setDescription] = useState('');
  const sendMemory = async () => {
    const memory = {
      name,
      price,
      capacity,
      size,
      description
    };
    try {
      await postMemory(memory);
    } catch (error) {
      alert('Memory post error');
    }
  };
  return (
    <div className="components-form">
      <span className="title-container">
        <h1>Create a Memory</h1>
      </span>
      <form className="form-container" onSubmit={sendMemory}>
        <label htmlFor="name">Name</label>
        <input
          className="name"
          type="name"
          value={name}
          onChange={event => setName(event.target.value)}
        />

        <label htmlFor="number">Price (EUR)</label>
        <input
          className="price"
          type="number"
          value={price}
          onChange={event => setPrice(event.target.value)}
          min={0}
        />

        <label htmlFor="number">Storage capacity (Go)</label>
        <input
          className="capacity"
          type="number"
          value={capacity}
          onChange={event => setCapacity(event.target.value)}
          min={0}
        />

        <label htmlFor="number">Size (mm)</label>
        <input
          className="size"
          type="number"
          value={size}
          onChange={event => setSize(event.target.value)}
          min={0}
        />

        <label htmlFor="text">Description</label>
        <textarea
          className="description"
          name="text"
          id="text"
          cols="30"
          rows="10"
          value={description}
          onChange={event => setDescription(event.target.value)}
        />
        <div className="send-button">
          <input type="submit" />
        </div>
      </form>
    </div>
  );
};

export default FormCreateMemory;
