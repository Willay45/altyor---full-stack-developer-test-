import React from 'react';
import { NavLink } from 'react-router-dom';
import './HomePage.css';

const HomePage = () => {
  return (
    <div className="home-page-container">
      <h1 className="home-page-title">My Memory List</h1>
      <NavLink
        activeClassName="active"
        to="/memories"
        className="link-container"
      >
        <p className="link-text">Connexion</p>
      </NavLink>
    </div>
  );
};

export default HomePage;
