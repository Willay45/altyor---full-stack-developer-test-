import React from 'react';
import Icon from '../utils/Icon';
import './MemoryCardComponents.css';
import { deleteMemory } from '../../api/Api';

const MemoryCardComponent = ({
  name,
  price,
  capacityStorage,
  size,
  description,
  id,
  create,
  setCreate,
  setSelectId
}) => {
  return (
    <div className="memory-card-container">
      <p>{name ? `${name}` : 'HyperX Fury'}</p>
      <p>{price ? `${price}` : '150'}</p>
      <p>{capacityStorage ? `${capacityStorage}` : '64'}</p>
      <p>{size ? `${size}` : '180'}</p>
      <p>
        {description
          ? `${description}`
          : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'}
      </p>
      <div className="button-container">
        <span className="update">
          <Icon
            image="fas fa-edit fa-2x"
            color="blue"
            onClick={() => {
              setCreate(!create);
              setSelectId(id);
            }}
          />
        </span>
        <span>
          <Icon
            image="fas fa-trash-alt fa-2x"
            color="red"
            onClick={async () => {
              await deleteMemory(id);
              window.location.reload(true);
            }}
          />
        </span>
      </div>
    </div>
  );
};

export default MemoryCardComponent;
