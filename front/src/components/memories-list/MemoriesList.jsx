import React, { useState, useEffect } from 'react';
import MemoryCardComponent from '../memory-card-component/MemoryCardComponent';
import './MemoriesList.css';
import FormCreateMemory from '../form-create-memory/FormCreateMemory';
import { getMemory } from '../../api/Api';
import FormUpdateMemory from '../form-update-memory/FormUpdateMemory';
import imgAltyor from '../assets/img/altyor.png';

const MemoriesList = () => {
  const [memories, setMemories] = useState([]);
  const [create, setCreate] = useState(true);
  const [selectId, setSelectId] = useState();

  const sortAlphabetical = () => {
    const memoriesCopy = memories.splice(0).sort((a, b) => {
      const x = a.name.toLowerCase();
      const y = b.name.toLowerCase();
      return x < y ? -1 : x > y ? 1 : 0;
    });
    setMemories(memoriesCopy);
  };

  const sortIncreasingPrice = () => {
    const memoriesCopy = memories.slice(0).sort((a, b) => a.price - b.price);
    setMemories(memoriesCopy);
  };

  const sortIncreasingCapacity = () => {
    const memoriesCopy = memories
      .slice(0)
      .sort((a, b) => a.capacity - b.capacity);
    setMemories(memoriesCopy);
  };

  const sortIncreasingSize = () => {
    const memoriesCopy = memories.slice(0).sort((a, b) => a.size - b.size);
    setMemories(memoriesCopy);
  };

  useEffect(() => {
    (async () => {
      setMemories(await getMemory());
    })();
  }, []);
  return (
    <div className="page-container">
      <div className="header-logo">
        <img src={imgAltyor} alt="logo-altyor" />
      </div>
      <div className="global-memory-list">
        <div className="header-container">
          <div onClick={() => sortAlphabetical()}>Name</div>
          <div onClick={() => sortIncreasingPrice()}>Price (EUR)</div>
          <div onClick={() => sortIncreasingCapacity()}>Capacity (Go)</div>
          <div onClick={() => sortIncreasingSize()}>Size (mm)</div>
          <div>Description</div>
        </div>
        <div className="memory-list">
          {memories.map((memory, index) => (
            <MemoryCardComponent
              description={memory.description}
              name={memory.name}
              price={memory.price}
              size={memory.size}
              capacityStorage={memory.capacity}
              id={memory.id}
              create={create}
              setCreate={setCreate}
              setSelectId={setSelectId}
              key={index}
            />
          ))}
        </div>
        <div className="global-form-container">
          {create ? (
            <FormCreateMemory />
          ) : (
            <FormUpdateMemory memoryId={selectId} />
          )}
        </div>
      </div>
    </div>
  );
};

export default MemoriesList;
