import React from 'react';
import './App.css';
import { Switch, Route } from 'react-router-dom';
import HomePage from './components/HomePage';
import MemoriesList from './components/memories-list/MemoriesList';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/memories" component={MemoriesList} />
      </Switch>
    </div>
  );
}

export default App;
