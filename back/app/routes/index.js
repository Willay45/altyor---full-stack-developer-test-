const express = require('express');

const memory = require('./memory.routes');

const router = express.Router();

router.use('/memories', memory);

module.exports = router;