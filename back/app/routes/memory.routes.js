const express = require('express');
const memory = require('../controllers/memory.controllers.js');
const controlInput = require('../middleware/controlInput.middleware');

const router = express.Router();

router.post('/', controlInput, memory.create);

router.get('/', memory.findAll);

router.put('/:memoryId', controlInput, memory.update);

router.delete('/:memoryId', memory.delete);

module.exports = router;
