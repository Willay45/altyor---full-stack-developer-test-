const Memory = require('../models/memory.models.js');

exports.create = (request, response) => {
  if (!request.body) {
    return response.status(400).send({
      message: 'Content con not be empty!'
    });
  }
  const memory = new Memory({
    name: request.body.name ? request.body.name : null,
    price: request.body.price ? request.body.price : null,
    description: request.body.description ? request.body.description : null,
    capacity: request.body.capacity ? request.body.capacity : null,
    size: request.body.size ? request.body.size : null
  });
  Memory.create(memory, (error, data) => {
    if (error) {
      return response.status(500).send({
        message:
          error.message || 'Some error occurred while creating the memory.'
      });
    }
    return response.send(data);
  });
};

exports.findAll = (request, response) => {
  Memory.findAll((error, data) => {
    if (error) {
      response.status(500).send({
        message: error.message || 'Some error occurred while retrieving memory.'
      });
    }
    return response.status(200).send(data);
  });
};

exports.update = (request, response) => {
  if (!request.body) {
    response.status(400).send({
      message: 'Content can not be empty!'
    });
  }

  Memory.update(
    request.params.memoryId,
    new Memory(request.body),
    (error, data) => {
      if (error) {
        if (error.kind === 'not_found') {
          response.status(404).send({
            message: `Not found memory with id ${request.params.memoryId}.`
          });
        } else {
          response.status(500).send({
            message: `Error updating memory with id ${request.params.memoryId}`
          });
        }
      } else {
        response.status(200).send(data);
      }
    }
  );
};

exports.delete = (request, response) => {
  Memory.delete(request.params.memoryId, error => {
    if (error) {
      if (error.kind === 'not_found') {
        response.status(404).send({
          message: `Not found memory with id ${request.params.memoryId}.`
        });
      } else {
        response.status(500).send({
          message: `Could not delete memory with id ${request.params.memoryId}`
        });
      }
    } else {
      response.send({ message: `memory was deleted successfully!` });
    }
  });
};
