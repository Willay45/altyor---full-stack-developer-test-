require('dotenv').config();
const app = require('express')();
const bodyParser = require('body-parser');
const cors = require('cors');
const api = require('./app/routes/index.js');

app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header({
    'Access-Control-Allow-Origin': '*'
  });
  next();
});

app.use(cors());

app.use('/api', api);

app.listen(process.env.PORT || 8080, () =>
  console.log(`Server is running on port ${process.env.PORT || 8080}.`)
);