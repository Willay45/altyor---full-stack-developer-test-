
#Express/React monorepo

##Initialisation

- Clonez ce repo
- Dans le dossier `back` crée un `.env` a coter de `server.js`
    
    PORT=8080
    
    DB_HOST=localhost
    
    DB_USER=root
    
    DB_PASSWORD=password
    
    DB_NAME=db_name
    
- Dans le dossier `front` allez dans le dossier `src` puis dans`api` et enfin dans le fichier `Api.js`.
puis renseigner votre adresse IP dans le variable `ip`

- Puis placez vous a la racine et executez :
    - `npm install`
    - `npx lerna bootstrap`

- Une fois fait executez : `npm start`

#Structure de la base de données
````
DROP DATABASE IF EXISTS `TestAltyor`;
CREATE DATABASE `TestAltyor`;
USE TestAltyor;
	

CREATE TABLE memory (
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	name VARCHAR(155) NOT NULL,
	price DECIMAL(8,3) NOT NULL,
	description TEXT NOT NULL,
	capacity INT NOT NULL,
	size DECIMAL(7,3) NOT NULL
) ENGINE = InnoDB;
    
    
INSERT INTO memory (name, price, capacity, size, description)
VALUES
('SanDisk - SDSQUAR-256G-GN6MA', 35.23, 256, 11, 'Carte Mémoire MicroSDHC Ultra 256GB avec Vitesse de Lecture Allant jusqu\'à 100MB/S Classe 10'),
('Samsung Mémoire Mb-Mc512Gaeu Evo Plus', 94.58, 512, 10, 'Carte Micro SD avec Adaptateur, Rouge/Blanc'),
('Carte Micro SD SDXC', 11, 256, 10, 'Carte Micro SD SDXC Haute Vitesse Classe 10 avec Adaptateur 256 Go'),
('Ruichicoo Carte Micro SD', 8, 32, 15, 'Ruichicoo Carte Micro SD 32 Go, carte mémoire haute vitesse de classe 10 avec adaptateur - V60');


````

#Info
Cette application a été réaliser en monorepo avec `lerna`, un outil d'analyse de code `ESlint`.

La partie Backend est réaliser avec `Express` et l'API est basé sur la philosophie `REST`.

La base de donnée a été réaliser avec `mysql`.

Quand a la partie frontend, elle a été réaliser `REACT`.