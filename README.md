![](./img/altyor.png)

# Altyor - Full stack developer test

Make sure you read **all** of this document carefully, and follow the guidelines in it.

## Context

Build a web-app where a user can add a memory component and see the list of existing memories.
We don't except a pixel perfect frontend make a simple and beautiful page that get the job done and that look similar to the mockup below. We don't mind the UI, as long the UX make sense.

## Mockup

![](./img/mockup.png)

## User Story

1. [ ] User open the webpage and could see a list of existing memory and a form to create a new one.
2. [ ] User can use the form to create a new memory component, the form will make sure that necessary information are input and under the right format BEFORE new memory is submitted to the API, the new memory component should appear in the list after successful creation.
3. [ ] A simple API will take care of the creation, modification and deletion of the memory component.
4. [ ] A list of all the existing memory component is show on the page.
    - [ ] The list can be sort by clicking on table header row.
    - [ ] A memory component present in the list can be modify and saved.
    - [ ] A memory component can be deleted.
5. [ ] Any change will be persistent in a database.
6. [ ] Any change **won't** need to be replicated if multi user are connected to the web-app in same time. (we are not building a real-time chat for example)

## Requirements

### Functionality

- The **frontend** part should be a single page application rendered in the frontend and load data from restful API (**not** rendered from backend).
- There should be a **backend** and database to store the memory components.

### Tech stack

- Backend
    - The API run on Node.js
    - You can use an API framework such as [Loopback](http://loopback.io/) or any Node.js module for data modeling, or nothing. It's your choice.
    - Try to orient your api about REST and CQRS philosophy.
    - Use the DB you like and feel that would be a good match for this story.
- Frontend
    - Use React for the frontend.
-Other
    - Write a clear readme documentation explain how to run the code.

### Bonus

- Write clear **documentation** on how it's designed and what the reason of your technical choice.
- Write good commit messages.
- An online demo is always welcome.
- For the frontend you can use a REACT UI library if you wish. (eg. Material UI or Ant design)


## What We Care About

Feel free to use any libraries you would use if this were a real production App, but remember we're interested in your code & the way you solve the problem, not how well you can use a particular library.

We're interested in your method and how you approach the problem just as much as we're interested in the end result.

Here's what you should aim for:

- Good use of current HTML, CSS, and JavaScript & performance best practices.
- Solid testing approach.
- Extensible code.
- Don't spend too much time on that exercise. If you decide to skip a part, make sure to let us know how you would have done it.

## Q&A

> Where should I send back the result when I'm done?

Fork this repo and send us a merge request when you think you are done. We don't have a deadline for the task. We will then maybe ask question on your merge request, don't be scare is part of the exercise.

> What if I have a question?

Create a new issue in the repo and we will get back to you very quickly.
